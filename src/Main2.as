package 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import net.hires.debug.Stats;
	import staling.StarlingManager;
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author jaiko
	 */
	[SWF(width="465", height="465", frameRate="60", backgroundColor="#000000")]
	public class Main2 extends Sprite 
	{
		private var background:Sprite;
		
		public function Main2():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			/*
			var g:Graphics;
			background = new Sprite();
			addChild(background);
			g = background.graphics;
			g.beginFill(0x121212);
			g.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			*/
			
			/*
			var circleManager:CircleManager = new CircleManager();
			addChild(circleManager);
			*/
			
			var _staling:Starling = new Starling(StarlingManager, stage);
			_staling.enableErrorChecking = false;
			_staling.showStats = true;
			_staling.start();
			
			var stats:Stats = new Stats();
			addChild(stats);
			stats.x = stage.stageWidth - stats.width;

		}
		

		
	}
	
}