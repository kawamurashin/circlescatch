package  
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class CirclePoint extends Sprite 
	{
		private var _insideDiameter:Number;
		private var _insideSpeed:Number = 0;
		public function CirclePoint() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{
			var g:Graphics;
			var bmd:BitmapData;
			var bm:Bitmap;
			var sprite:Sprite = new Sprite();
			g = sprite.graphics;
			g.beginFill(0xFFFFFF);
			g.drawRect(0, 0, 1, 1);
			bmd = new BitmapData(1, 1);
			bmd.draw(sprite);
			bm = new Bitmap(bmd, "auto", false);
			addChild(bm);
			bm.x = 0;
			bm.y = 0;
			
		}
		
		public function get insideDiameter():Number 
		{
			return _insideDiameter;
		}
		
		public function set insideDiameter(value:Number):void 
		{
			_insideDiameter = value;
		}
		
		public function get insideSpeed():Number 
		{
			return _insideSpeed;
		}
		
		public function set insideSpeed(value:Number):void 
		{
			_insideSpeed = value;
		}
		
	}

}