package  
{
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

/**
	 * ...
	 * @author jaiko
	 */
	public class Circle extends Sprite 
	{
		private const RADIUS:Number = 40;
		private var rotationalSpeed:Number;
		private var constantRotationalSpeed:Number;
		private var velocityRotationalSpeed:Number = 0;
		private var random:Number;
		private var multiple:uint;
		private var insideDiameter:Number;
        //
        private var timer:Timer;
		private var pointList:Array;
		
		public function Circle() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		public function onEnterFrame():void
		{
			var i:uint;
			var n:uint;
			var circlePoint:CirclePoint;
			/**/
			var theta:Number;
			var _x:Number;
			var _y:Number;
			var r:Number 
			var radius:Number;
            //
            var aInside:Number;
            var inside:Number;
			//
			velocityRotationalSpeed *= 0.95;
			rotationalSpeed += constantRotationalSpeed + velocityRotationalSpeed;
			//
			n = pointList.length;
			for (i = 0; i < n; i++)
			{
				circlePoint = pointList[i];
				aInside = 0.1 *( insideDiameter - circlePoint.insideDiameter)
				circlePoint.insideSpeed += aInside - 0.1 * circlePoint.insideSpeed;
				
				circlePoint.insideDiameter += circlePoint.insideSpeed;
				
				
				theta = random * (2 * Math.PI * (i / n)) + rotationalSpeed;
				
				r = RADIUS * ((1 + Math.cos(multiple * 2 * Math.PI * (i / n))) * 0.5);
				radius = (RADIUS - circlePoint.insideDiameter) * ( r / RADIUS) + circlePoint.insideDiameter;
				_x = radius * Math.cos(theta);
				_y = radius * Math.sin(theta);
				//
				circlePoint.x = _x;
				circlePoint.y = _y;
			}
		}
		
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{
			var i:uint;
			var n:uint;
			
			var circlePoint:CirclePoint;
			var container:Sprite = new Sprite();
			addChild(container);

			
			random = 100 * Math.random();
			random *= random;
			random = Math.floor(random);

			insideDiameter = 30 * Math.random();
			
			rotationalSpeed = 0;
			constantRotationalSpeed = (0.01 + 0.99 * Math.random()) * 0.03;
			
			multiple = Math.floor(50 * Math.random()) +1;
			
			pointList = [];
			n = 180;
			for (i = 0; i < n; i++)
			{
				circlePoint = new CirclePoint();
				
				container.addChild(circlePoint);
				circlePoint.insideDiameter = insideDiameter;
				circlePoint.insideSpeed = 0;
				pointList[i] = circlePoint;
			}
            timer = new Timer(2000, 1);
            timer.addEventListener(TimerEvent.TIMER , timerHandler);
            timer.start();
        }
		
        private function timerHandler(event:TimerEvent):void {
			var i:uint;
			var n:uint;
			var circlePoint:CirclePoint;
            var power:Number = 0.1 + 0.05 * Math.random();
            velocityRotationalSpeed =  power;
            
			n = pointList.length;
			for (i = 0; i < n; i++)
			{
				circlePoint = pointList[i];
				circlePoint.insideSpeed += power * 50; 
			}
            timer = new Timer(2000, 1);
            timer.addEventListener(TimerEvent.TIMER , timerHandler);
            timer.start();
        }
}

}