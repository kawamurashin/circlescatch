package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class CircleManager extends Sprite 
	{
		
		private const OFFSET:Number = 100;
		private var cirlceList:Array;
		public function CircleManager() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{
			var i:uint;
			var n:uint;
			var circle:Circle;
			
			var _w:uint = Math.floor(stage.stageWidth / OFFSET) + 1;
			var _h:uint = (stage.stageHeight / OFFSET)+ 2;
			var sx:Number = 0.5* OFFSET + ((OFFSET * _w) - stage.stageWidth) * -0.5;
			var sy:Number = 0.5 * OFFSET + ( (OFFSET * _h ) - stage.stageHeight ) * -0.5;
			cirlceList = [];
			n =  (_w * _h);
			for (i = 0; i < n; i++)
			{
				circle = new Circle();
				addChild(circle);
				circle.x = sx + OFFSET * (i % _w);
				circle.y = sy + OFFSET * Math.floor(i / _w);
				circle.onEnterFrame();
				cirlceList.push(circle);
			}
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		private function enterFrameHandler(e:Event):void 
		{
			var i:uint;
			var n:uint;
			var circle:Circle;
			//
			n = cirlceList.length;
			for (i = 0; i < n; i++)
			{
				circle = cirlceList[i];
				circle.onEnterFrame();
			}
		}
	}

}