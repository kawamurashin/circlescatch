package staling {
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import starling.display.Image;
	/**
	 * ...
	 * @author jaiko
	 */
	public class StarlingPoint 
	{
		
		private var _x:Number;
		private var _y:Number;
		private var _insideDiameter:Number;
		private var _insideSpeed:Number = 0;
		
		private var _starlingCircle:StarlingCircle;
		private var _image:Image;
		//
		private var _power:Number;
		private var timer:Timer
		
		public function StarlingPoint() 
		{

		}
		public function start():void
		{
			var d:Number = Math.sqrt(Math.pow(_x, 2)　 +　Math.pow(_y, 2)) -_insideDiameter;
			if (d < 0)
			{
				d = 0;
			}
			d *= 40;
			timer = new Timer(d, 1);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, timerCompleteHandler);
		}
		public function get x():Number 
		{
			return _x;
		}
		
		public function set x(value:Number):void 
		{
			_x = value;
		}
		
		public function get y():Number 
		{
			return _y;
		}
		
		public function set y(value:Number):void 
		{
			_y = value;
		}
		
		public function get starlingCircle():StarlingCircle 
		{
			return _starlingCircle;
		}
		
		public function set starlingCircle(value:StarlingCircle):void 
		{
			_starlingCircle = value;
		}
		
		public function get image():Image 
		{
			return _image;
		}
		
		public function set image(value:Image):void 
		{
			_image = value;
		}
		
		public function get insideDiameter():Number 
		{
			return _insideDiameter;
		}
		
		public function set insideDiameter(value:Number):void 
		{
			_insideDiameter = value;
		}
		
		public function get insideSpeed():Number 
		{
			return _insideSpeed;
		}
		
		public function set insideSpeed(value:Number):void 
		{
			_insideSpeed = value;
		}
		
		public function set power(value:Number):void 
		{
			_power = value;
			
			//_insideSpeed += _power * 50;
			timer.start();
		}
		
		private function timerCompleteHandler(e:TimerEvent):void 
		{
			_insideSpeed += _power * 50;
		}
		
	}

}