package staling {
	import flash.events.TimerEvent;
	import flash.utils.Timer;

	

	
	/**
	 * ...
	 * @author jaiko
	 */
	public class StarlingCircle
	{
		private const RADIUS:Number = 40;
		private var rotationalSpeed:Number;
		private var constantRotationalSpeed:Number;
		private var velocityRotationalSpeed:Number = 0;
		private var random:Number;
		private var multiple:uint;
		private var insideDiameter:Number;
		private var delay:Number;
		//starling
		private var _x:Number;
		private var _y:Number;
		private var _pointList:Array;
		private var timer:Timer;
		
		
		
		
		public function StarlingCircle() 
		{
			super();
			layout();
		}
		public function onEnterFrame():void
		{
			var i:uint;
			var n:uint;
			var circlePoint:CirclePoint;
			/**/
			var theta:Number;
			var _x:Number;
			var _y:Number;
			var r:Number 
			var radius:Number;
            //
            var aInside:Number;
            var inside:Number;
			var starlingPoint:StarlingPoint;
			//
			velocityRotationalSpeed *= 0.95;
			rotationalSpeed += constantRotationalSpeed + velocityRotationalSpeed;
			//
			n = _pointList.length;
			for (i = 0; i < n; i++)
			{
				starlingPoint = _pointList[i];
				aInside = 0.1 *( insideDiameter - starlingPoint.insideDiameter)
				starlingPoint.insideSpeed += aInside - 0.05 * starlingPoint.insideSpeed;
				
				starlingPoint.insideDiameter += starlingPoint.insideSpeed;
				
				
				theta = random * (2 * Math.PI * (i / n)) + rotationalSpeed;
				
				r = RADIUS * ((1 + Math.cos(multiple * 2 * Math.PI * (i / n))) * 0.5);
				radius = (RADIUS - starlingPoint.insideDiameter) * ( r / RADIUS) + starlingPoint.insideDiameter;
				_x = radius * Math.cos(theta);
				_y = radius * Math.sin(theta);
				//
				starlingPoint.x = _x;
				starlingPoint.y = _y;
			}
		}
		
		public function start():void 
		{
			var i:uint;
			var n:uint;
			var starlingPoint:StarlingPoint;
			
			delay = 2000
			var d:Number = Math.sqrt(Math.pow(465 * 0.5 - _x, 2) + Math.pow(465 * 0.5 - _y, 2));
			timer = new Timer(delay + d *1.5 , 1);
            timer.addEventListener(TimerEvent.TIMER , timerHandler);
            timer.start();
			
			n = _pointList.length;
			for (i = 0; i < n; i++)
			{
				starlingPoint = _pointList[i];
				starlingPoint.start();
			}
		}

		
		private function layout():void 
		{
			var i:uint;
			var n:uint;
			var theta:Number;
			var radius:Number;
			var starlingPoint:StarlingPoint;
			//
			random = 100 * Math.random();
			random *= random;
			random = Math.floor(random);

			insideDiameter = 30 * Math.random();
			
			rotationalSpeed = 0;
			constantRotationalSpeed = (0.0 + 1.0 * Math.random()) * 0.03;
			
			multiple = Math.floor(50 * Math.random()) +1;
			//
			_pointList = [];
			n = 546;
			for (i = 0; i < n; i++)
			{
				starlingPoint = new StarlingPoint();
				
				starlingPoint.insideDiameter = insideDiameter;
				starlingPoint.insideSpeed = 0;
				
				starlingPoint.starlingCircle = this;
				_pointList[i] = starlingPoint;
			}
			
			onEnterFrame();
			

			
		}
        private function timerHandler(event:TimerEvent):void {
			var i:uint;
			var n:uint;
			var starlingPoint:StarlingPoint;
            var power:Number = 0.1 + 0.05 * Math.random();
            velocityRotationalSpeed =  power;
            
			n = _pointList.length;
			for (i = 0; i < n; i++)
			{
				starlingPoint = _pointList[i];
				//starlingPoint.insideSpeed += power * 50; 
				starlingPoint.power = power;
			}
			
            timer = new Timer(delay, 1);
            timer.addEventListener(TimerEvent.TIMER , timerHandler);
            timer.start();
        }
		
		public function get y():Number 
		{
			return _y;
		}
		
		public function set y(value:Number):void 
		{
			_y = value;
		}
		
		public function get x():Number 
		{
			return _x;
		}
		
		public function set x(value:Number):void 
		{
			_x = value;
		}
		
		public function get pointList():Array 
		{
			return _pointList;
		}
		
	}

}