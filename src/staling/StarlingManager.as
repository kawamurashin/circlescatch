package staling {
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.EnterFrameEvent;
	import starling.events.Event;
	import starling.textures.Texture;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class StarlingManager extends Sprite 
	{
		private const OFFSET:Number = 100;
		//
		private var circleList:Vector.<StarlingCircle> = new Vector.<StarlingCircle>();

		private var starlingPointList:Vector.<StarlingPoint> = new Vector.<StarlingPoint>();
		
		public function StarlingManager() 
		{
			super();
			
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE,init)
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{
			var circle:StarlingCircle;
			var i:uint;
			var j:uint;
			var n:uint;
			var m:uint;
			var starlingPoint:StarlingPoint;
			var pointList:Array;
			
			var bmd:BitmapData;
			var image:Image;
			var texture:Texture;

			
			var sprite:flash.display.Sprite = new flash.display.Sprite();
			var g:Graphics = sprite.graphics;
			g.beginFill(0xFFFFFF);
			g.drawRect(0, 0, 1, 1);
			bmd = new BitmapData(sprite.width, sprite.height);
			bmd.draw(sprite);
			texture = Texture.fromBitmapData(bmd);

			var _w:uint = Math.floor(stage.stageWidth / OFFSET) + 1;
			var _h:uint = (stage.stageHeight / OFFSET) + 2;
			var sx:Number = 0.5* OFFSET + ((OFFSET * _w) - stage.stageWidth) * -0.5;
			var sy:Number = 0.5 * OFFSET + ( (OFFSET * _h ) - stage.stageHeight ) * -0.5;
			
			
			n =  (_w * _h);
			for (i = 0; i < n; i++)
			{
				circle = new StarlingCircle();
				
				circle.x = sx + OFFSET * (i % _w);
				circle.y = sy + OFFSET * Math.floor(i / _w);
				circle.start();
				circleList[i] = circle;
				
				pointList = circle.pointList;
				m = pointList.length;
				for (j = 0; j < m; j++)
				{
					starlingPoint = pointList[j];
					starlingPointList.push(starlingPoint);
					
					image = new Image(texture);
					addChild(image);
					image.x = circle.x + starlingPoint.x;
					image.y = circle.y + starlingPoint.y;
					
					starlingPoint.image = image;
				}
			}
			addEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
		}
		
		private function enterFrameHandler(e:EnterFrameEvent):void 
		{
			var circle:StarlingCircle;
			var i:uint;
			var j:uint;
			var n:uint;
			var m:uint;
			var starlingPoint:StarlingPoint;
			var pointList:Array;
			var image:Image;
			
			n = circleList.length;
			for (i = 0; i < n; i++)
			{
				circle = circleList[i];
				circle.onEnterFrame();
			}
			
			n = starlingPointList.length;
			for (i = 0; i < n; i++)
			{
				starlingPoint = starlingPointList[i];
				image = starlingPoint.image;
				circle = starlingPoint.starlingCircle;
				
				image.x = circle.x + starlingPoint.x;
				image.y = circle.y + starlingPoint.y;
			}
		}
		
	}

}