package  
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import net.hires.debug.Stats;
	import starling.core.Starling;
	
	/**
	 * ...
	 * @author jaiko
	 */
	[SWF(width="465", height="465", frameRate="60", backgroundColor="#000000")]
	public class Main extends flash.display.Sprite 
	{
		
		public function Main() 
		{
			super();
			
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			/*
			var g:Graphics;
			background = new Sprite();
			addChild(background);
			g = background.graphics;
			g.beginFill(0x121212);
			g.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			*/
			
			/*
			var circleManager:CircleManager = new CircleManager();
			addChild(circleManager);
			*/
			
			var _staling:Starling = new Starling(StarlingManager, stage);
			_staling.enableErrorChecking = false;
			_staling.showStats = true;
			_staling.start();
			
			var stats:Stats = new Stats();
			addChild(stats);
			stats.x = stage.stageWidth - stats.width;
		}
		
	}

}

import flash.display.BitmapData;
import flash.display.Graphics;
import starling.display.Image;
import starling.display.Sprite;
import starling.events.EnterFrameEvent;
import starling.events.Event;
import starling.textures.Texture;

/**
 * ...
 * @author jaiko
 */
class StarlingManager extends Sprite 
{
	private const OFFSET:Number = 100;
	//
	private var circleList:Vector.<StarlingCircle> = new Vector.<StarlingCircle>();

	private var starlingPointList:Vector.<StarlingPoint> = new Vector.<StarlingPoint>();
	
	public function StarlingManager() 
	{
		super();
		
		if (stage) init(null);
		else addEventListener(Event.ADDED_TO_STAGE,init)
	}
	
	private function init(e:Event):void 
	{
		removeEventListener(Event.ADDED_TO_STAGE, init);
		//
		layout();
	}
	
	private function layout():void 
	{
		var circle:StarlingCircle;
		var i:uint;
		var j:uint;
		var n:uint;
		var m:uint;
		var starlingPoint:StarlingPoint;
		var pointList:Array;
		
		var bmd:BitmapData;
		var image:Image;
		var texture:Texture;

		
		var sprite:flash.display.Sprite = new flash.display.Sprite();
		var g:Graphics = sprite.graphics;
		g.beginFill(0xFFFFFF);
		g.drawRect(0, 0, 1, 1);
		bmd = new BitmapData(sprite.width, sprite.height);
		bmd.draw(sprite);
		texture = Texture.fromBitmapData(bmd);

		var _w:uint = Math.floor(stage.stageWidth / OFFSET) + 1;
		var _h:uint = (stage.stageHeight / OFFSET) + 2;
		var sx:Number = 0.5* OFFSET + ((OFFSET * _w) - stage.stageWidth) * -0.5;
		var sy:Number = 0.5 * OFFSET + ( (OFFSET * _h ) - stage.stageHeight ) * -0.5;
		
		
		n =  (_w * _h);
		for (i = 0; i < n; i++)
		{
			circle = new StarlingCircle();
			
			circle.x = sx + OFFSET * (i % _w);
			circle.y = sy + OFFSET * Math.floor(i / _w);
			circle.start();
			circleList[i] = circle;
			
			pointList = circle.pointList;
			m = pointList.length;
			for (j = 0; j < m; j++)
			{
				starlingPoint = pointList[j];
				starlingPointList.push(starlingPoint);
				
				image = new Image(texture);
				addChild(image);
				image.x = circle.x + starlingPoint.x;
				image.y = circle.y + starlingPoint.y;
				
				starlingPoint.image = image;
			}
		}
		addEventListener(EnterFrameEvent.ENTER_FRAME, enterFrameHandler);
	}
	
	private function enterFrameHandler(e:EnterFrameEvent):void 
	{
		var circle:StarlingCircle;
		var i:uint;
		var j:uint;
		var n:uint;
		var m:uint;
		var starlingPoint:StarlingPoint;
		var pointList:Array;
		var image:Image;
		
		n = circleList.length;
		for (i = 0; i < n; i++)
		{
			circle = circleList[i];
			circle.onEnterFrame();
		}
		
		n = starlingPointList.length;
		for (i = 0; i < n; i++)
		{
			starlingPoint = starlingPointList[i];
			image = starlingPoint.image;
			circle = starlingPoint.starlingCircle;
			
			image.x = circle.x + starlingPoint.x;
			image.y = circle.y + starlingPoint.y;
		}
	}
	
}


import flash.events.TimerEvent;
import flash.utils.Timer;




/**
 * ...
 * @author jaiko
 */
class StarlingCircle
{
	private const RADIUS:Number = 40;
	private var rotationalSpeed:Number;
	private var constantRotationalSpeed:Number;
	private var velocityRotationalSpeed:Number = 0;
	private var random:Number;
	private var multiple:uint;
	private var insideDiameter:Number;
	private var delay:Number;
	//starling
	private var _x:Number;
	private var _y:Number;
	private var _pointList:Array;
	private var timer:Timer;
	
	
	
	
	public function StarlingCircle() 
	{
		super();
		layout();
	}
	public function onEnterFrame():void
	{
		var i:uint;
		var n:uint;
		var circlePoint:CirclePoint;
		/**/
		var theta:Number;
		var _x:Number;
		var _y:Number;
		var r:Number 
		var radius:Number;
		//
		var aInside:Number;
		var inside:Number;
		var starlingPoint:StarlingPoint;
		//
		velocityRotationalSpeed *= 0.95;
		rotationalSpeed += constantRotationalSpeed + velocityRotationalSpeed;
		//
		n = _pointList.length;
		for (i = 0; i < n; i++)
		{
			starlingPoint = _pointList[i];
			aInside = 0.1 *( insideDiameter - starlingPoint.insideDiameter)
			starlingPoint.insideSpeed += aInside - 0.05 * starlingPoint.insideSpeed;
			
			starlingPoint.insideDiameter += starlingPoint.insideSpeed;
			
			
			theta = random * (2 * Math.PI * (i / n)) + rotationalSpeed;
			
			r = RADIUS * ((1 + Math.cos(multiple * 2 * Math.PI * (i / n))) * 0.5);
			radius = (RADIUS - starlingPoint.insideDiameter) * ( r / RADIUS) + starlingPoint.insideDiameter;
			_x = radius * Math.cos(theta);
			_y = radius * Math.sin(theta);
			//
			starlingPoint.x = _x;
			starlingPoint.y = _y;
		}
	}
	
	public function start():void 
	{
		var i:uint;
		var n:uint;
		var starlingPoint:StarlingPoint;
		
		delay = 2000
		var d:Number = Math.sqrt(Math.pow(465 * 0.5 - _x, 2) + Math.pow(465 * 0.5 - _y, 2));
		timer = new Timer(delay + d *1.5 , 1);
		timer.addEventListener(TimerEvent.TIMER , timerHandler);
		timer.start();
		
		n = _pointList.length;
		for (i = 0; i < n; i++)
		{
			starlingPoint = _pointList[i];
			starlingPoint.start();
		}
	}

	
	private function layout():void 
	{
		var i:uint;
		var n:uint;
		var theta:Number;
		var radius:Number;
		var starlingPoint:StarlingPoint;
		//
		random = 100 * Math.random();
		random *= random;
		random = Math.floor(random);

		insideDiameter = 30 * Math.random();
		
		rotationalSpeed = 0;
		constantRotationalSpeed = (0.0 + 1.0 * Math.random()) * 0.03;
		
		multiple = Math.floor(50 * Math.random()) +1;
		//
		_pointList = [];
		n = 360;
		for (i = 0; i < n; i++)
		{
			starlingPoint = new StarlingPoint();
			
			starlingPoint.insideDiameter = insideDiameter;
			starlingPoint.insideSpeed = 0;
			
			starlingPoint.starlingCircle = this;
			_pointList[i] = starlingPoint;
		}
		
		onEnterFrame();
		

		
	}
	private function timerHandler(event:TimerEvent):void {
		var i:uint;
		var n:uint;
		var starlingPoint:StarlingPoint;
		var power:Number = 0.1 + 0.05 * Math.random();
		velocityRotationalSpeed =  power;
		
		n = _pointList.length;
		for (i = 0; i < n; i++)
		{
			starlingPoint = _pointList[i];
			//starlingPoint.insideSpeed += power * 50; 
			starlingPoint.power = power;
		}
		
		timer = new Timer(delay, 1);
		timer.addEventListener(TimerEvent.TIMER , timerHandler);
		timer.start();
	}
	
	public function get y():Number 
	{
		return _y;
	}
	
	public function set y(value:Number):void 
	{
		_y = value;
	}
	
	public function get x():Number 
	{
		return _x;
	}
	
	public function set x(value:Number):void 
	{
		_x = value;
	}
	
	public function get pointList():Array 
	{
		return _pointList;
	}
	
}

import flash.events.TimerEvent;
import flash.utils.Timer;
import starling.display.Image;
/**
 * ...
 * @author jaiko
 */
class StarlingPoint 
{
	
	private var _x:Number;
	private var _y:Number;
	private var _insideDiameter:Number;
	private var _insideSpeed:Number = 0;
	
	private var _starlingCircle:StarlingCircle;
	private var _image:Image;
	//
	private var _power:Number;
	private var timer:Timer
	
	public function StarlingPoint() 
	{

	}
	public function start():void
	{
		var d:Number = Math.sqrt(Math.pow(_x, 2)　 +　Math.pow(_y, 2)) -_insideDiameter;
		if (d < 0)
		{
			d = 0;
		}
		d *= 40;
		timer = new Timer(d, 1);
		timer.addEventListener(TimerEvent.TIMER_COMPLETE, timerCompleteHandler);
	}
	public function get x():Number 
	{
		return _x;
	}
	
	public function set x(value:Number):void 
	{
		_x = value;
	}
	
	public function get y():Number 
	{
		return _y;
	}
	
	public function set y(value:Number):void 
	{
		_y = value;
	}
	
	public function get starlingCircle():StarlingCircle 
	{
		return _starlingCircle;
	}
	
	public function set starlingCircle(value:StarlingCircle):void 
	{
		_starlingCircle = value;
	}
	
	public function get image():Image 
	{
		return _image;
	}
	
	public function set image(value:Image):void 
	{
		_image = value;
	}
	
	public function get insideDiameter():Number 
	{
		return _insideDiameter;
	}
	
	public function set insideDiameter(value:Number):void 
	{
		_insideDiameter = value;
	}
	
	public function get insideSpeed():Number 
	{
		return _insideSpeed;
	}
	
	public function set insideSpeed(value:Number):void 
	{
		_insideSpeed = value;
	}
	
	public function set power(value:Number):void 
	{
		_power = value;
		
		//_insideSpeed += _power * 50;
		timer.start();
	}
	
	private function timerCompleteHandler(e:TimerEvent):void 
	{
		_insideSpeed += _power * 50;
	}
	
}

